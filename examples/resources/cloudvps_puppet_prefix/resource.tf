resource "cloudvps_puppet_prefix" "web" {
  name = "someproject-web-"

  roles = [
    "profile::someproject::web"
  ]

  hiera = file("hiera_web.yaml")
}
