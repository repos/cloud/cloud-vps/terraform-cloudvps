resource "cloudvps_web_proxy" "foo_app" {
  hostname = "foo"
  backends = ["http://192.51.100.2:8000"]
}

# If you want to reference the IP of a host you deployed directly that
# can be done as follows
resource "cloudvps_web_proxy" "web_proxy" {
  hostname = "example"
  backends = ["http://${resource.openstack_compute_instance_v2.vm.access_ip_v4}:8888"]
}
