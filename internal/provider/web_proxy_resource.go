package provider

import (
	"context"
	"fmt"
	"strings"

	"github.com/gophercloud/gophercloud"
	"github.com/hashicorp/terraform-plugin-framework-validators/listvalidator"
	"github.com/hashicorp/terraform-plugin-framework/attr"
	"github.com/hashicorp/terraform-plugin-framework/diag"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/planmodifier"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/stringplanmodifier"
	"github.com/hashicorp/terraform-plugin-framework/schema/validator"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"

	cloudvps "gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/clients"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/webproxy"
)

var _ resource.Resource = &webProxyResource{}
var _ resource.ResourceWithImportState = &webProxyResource{}

func NewWebProxyResource() resource.Resource {
	return &webProxyResource{}
}

type webProxyResource struct {
	data *CloudVpsProviderData
}

type webProxyModel struct {
	Hostname types.String `tfsdk:"hostname"`
	Domain   types.String `tfsdk:"domain"`
	Backends types.List   `tfsdk:"backends"`
}

func (r *webProxyResource) Metadata(ctx context.Context, req resource.MetadataRequest, resp *resource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_web_proxy"
}

func (r *webProxyResource) Schema(ctx context.Context, req resource.SchemaRequest, resp *resource.SchemaResponse) {
	resp.Schema = schema.Schema{
		MarkdownDescription: "Manages a Cloud VPS [web proxy](https://wikitech.wikimedia.org/wiki/Help:Proxy).",

		Attributes: map[string]schema.Attribute{
			"hostname": schema.StringAttribute{
				MarkdownDescription: "Public host name",
				Required:            true,
				PlanModifiers: []planmodifier.String{
					stringplanmodifier.RequiresReplace(),
				},
			},
			"domain": schema.StringAttribute{
				MarkdownDescription: "Public domain name",
				Optional:            true,
				Computed:            true,
				PlanModifiers: []planmodifier.String{
					stringplanmodifier.UseStateForUnknown(),
					stringplanmodifier.RequiresReplace(),
				},
			},
			"backends": schema.ListAttribute{
				MarkdownDescription: "HTTP backends",
				Required:            true,
				ElementType:         types.StringType,
				Validators: []validator.List{
					listvalidator.SizeAtLeast(1),
					// for now. behaviour with multiple proxies hasn't been tested yet
					listvalidator.SizeAtMost(1),
				},
			},
		},
	}
}

func (r *webProxyResource) Configure(ctx context.Context, req resource.ConfigureRequest, resp *resource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	data, ok := req.ProviderData.(*CloudVpsProviderData)

	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Resource Configure Type",
			fmt.Sprintf("Expected *CloudVpsProviderData, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	r.data = data
}

func getFqdn(model webProxyModel, client *webproxy.Client) string {
	domain := ""
	if model.Domain.IsUnknown() || model.Domain.IsNull() {
		defaultDomain := client.ListDomains().Default()
		domain = defaultDomain.Name
	} else {
		domain = model.Domain.ValueString()
	}

	for strings.HasSuffix(domain, ".") {
		domain = strings.TrimSuffix(domain, ".")
	}

	return fmt.Sprintf("%v.%v", model.Hostname.ValueString(), domain)
}

func getBackends(data webProxyModel) []string {
	var backends []string
	for _, elem := range data.Backends.Elements() {
		backends = append(backends, elem.(types.String).ValueString())
	}
	return backends
}

func proxyToData(proxy webproxy.Proxy) (*webProxyModel, diag.Diagnostics) {
	splitHostname := strings.SplitN(proxy.Domain, ".", 2)

	var backends []attr.Value
	for _, backend := range proxy.Backends {
		backends = append(backends, types.StringValue(backend))
	}

	backendList, diags := types.ListValue(types.StringType, backends)

	result := &webProxyModel{
		Hostname: types.StringValue(splitHostname[0]),
		Domain:   types.StringValue(splitHostname[1]),
		Backends: backendList,
	}

	return result, diags
}

func (r *webProxyResource) Create(ctx context.Context, req resource.CreateRequest, resp *resource.CreateResponse) {
	var data webProxyModel

	diags := req.Config.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)

	if resp.Diagnostics.HasError() {
		return
	}

	proxyClient, err := cloudvps.NewWebProxyClient(
		r.data.OsClient,
		gophercloud.EndpointOpts{})

	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to create client",
			fmt.Sprintf("Unable to create web proxy client: %v", err),
		)
		return
	}

	fqdn := getFqdn(data, proxyClient)
	backends := getBackends(data)
	proxy := webproxy.Proxy{
		Domain:   fqdn,
		Backends: backends,
	}

	response := proxyClient.CreateProxy(&proxy)
	if response.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to create proxy",
			fmt.Sprintf("Unable to create a new web proxy for %v: %v", fqdn, response.Err),
		)
		return
	}

	tflog.Trace(ctx, "created a resource")

	newData, diags := proxyToData(proxy)
	resp.Diagnostics.Append(diags...)

	diags = resp.State.Set(ctx, newData)
	resp.Diagnostics.Append(diags...)
}

func (r *webProxyResource) Read(ctx context.Context, req resource.ReadRequest, resp *resource.ReadResponse) {
	var data webProxyModel

	diags := req.State.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)

	if resp.Diagnostics.HasError() {
		return
	}

	proxyClient, err := cloudvps.NewWebProxyClient(
		r.data.OsClient,
		gophercloud.EndpointOpts{})

	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to create client",
			fmt.Sprintf("Unable to create web proxy client: %v", err),
		)
		return
	}

	fqdn := getFqdn(data, proxyClient)
	response := proxyClient.FindProxy(fqdn)
	if response.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to get proxy data",
			fmt.Sprintf("Unable to load data for web proxy %v: %v", fqdn, response.Err),
		)
		return
	}

	proxy, err := response.Proxy()
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to get proxy data",
			fmt.Sprintf("Unable to parse web proxy %v: %v", fqdn, err),
		)
		return
	}

	newData, diags := proxyToData(proxy)
	resp.Diagnostics.Append(diags...)

	diags = resp.State.Set(ctx, newData)
	resp.Diagnostics.Append(diags...)
}

func (r *webProxyResource) Update(ctx context.Context, req resource.UpdateRequest, resp *resource.UpdateResponse) {
	var data webProxyModel
	diags := req.Plan.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)

	if resp.Diagnostics.HasError() {
		return
	}

	proxyClient, err := cloudvps.NewWebProxyClient(
		r.data.OsClient,
		gophercloud.EndpointOpts{})

	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to create client",
			fmt.Sprintf("Unable to create web proxy client: %v", err),
		)
		return
	}

	fqdn := getFqdn(data, proxyClient)
	response := proxyClient.FindProxy(fqdn)
	if response.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to get proxy data",
			fmt.Sprintf("Unable to load data for web proxy %v: %v", fqdn, response.Err),
		)
		return
	}

	proxy, err := response.Proxy()
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to get proxy data",
			fmt.Sprintf("Unable to parse web proxy %v: %v", fqdn, err),
		)
		return
	}

	proxy.Domain = fqdn
	proxy.Backends = getBackends(data)

	updateResponse := proxyClient.UpdateProxy(&proxy)
	if updateResponse.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to update proxy",
			fmt.Sprintf("Unable to update web proxy %v: %v", fqdn, updateResponse.Err),
		)
		return
	}

	newData, diags := proxyToData(proxy)
	resp.Diagnostics.Append(diags...)

	diags = resp.State.Set(ctx, newData)
	resp.Diagnostics.Append(diags...)
}

func (r *webProxyResource) Delete(ctx context.Context, req resource.DeleteRequest, resp *resource.DeleteResponse) {
	var data webProxyModel

	diags := req.State.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)

	if resp.Diagnostics.HasError() {
		return
	}

	proxyClient, err := cloudvps.NewWebProxyClient(
		r.data.OsClient,
		gophercloud.EndpointOpts{})

	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to create client",
			fmt.Sprintf("Unable to create web proxy client: %v", err),
		)
		return
	}

	fqdn := getFqdn(data, proxyClient)
	response := proxyClient.FindProxy(fqdn)
	if response.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to get proxy data",
			fmt.Sprintf("Unable to load data for web proxy %v: %v", fqdn, response.Err),
		)
		return
	}

	proxy, err := response.Proxy()
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to get proxy data",
			fmt.Sprintf("Unable to parse web proxy %v: %v", fqdn, err),
		)
		return
	}

	updateResponse := proxyClient.DeleteProxy(&proxy)
	if updateResponse.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to delete proxy",
			fmt.Sprintf("Unable to delete web proxy %v: %v", fqdn, updateResponse.Err),
		)
		return
	}
}

func (r *webProxyResource) ImportState(ctx context.Context, req resource.ImportStateRequest, resp *resource.ImportStateResponse) {
	splitHostname := strings.SplitN(req.ID, ".", 2)

	if len(splitHostname) != 2 {
		resp.Diagnostics.AddError(
			"Unable to import proxy",
			"Received invalid name as an input",
		)

		return
	}

	if strings.HasSuffix(splitHostname[1], ".") {
		splitHostname[1] = strings.TrimRight(splitHostname[1], ".")
	}

	if splitHostname[1] == "" {
		resp.Diagnostics.AddError(
			"Unable to import proxy",
			"Received invalid name as an input",
		)

		return
	}

	diagnostics := resp.State.SetAttribute(ctx, path.Root("hostname"), splitHostname[0])
	resp.Diagnostics.Append(diagnostics...)

	diagnostics = resp.State.SetAttribute(ctx, path.Root("domain"), splitHostname[1])
	resp.Diagnostics.Append(diagnostics...)
}
