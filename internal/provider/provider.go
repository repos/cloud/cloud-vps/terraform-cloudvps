package provider

import (
	"context"
	"fmt"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack"
	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/provider"
	"github.com/hashicorp/terraform-plugin-framework/provider/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/types"
)

// Ensure provider defined types fully satisfy framework interfaces
var _ provider.Provider = &CloudVpsProvider{}

// CloudVpsProvider defines the provider implementation.
type CloudVpsProvider struct {
	// version is set to the provider version on release, "dev" when the
	// provider is built and ran locally, and "test" when running acceptance
	// testing.
	version string
}

// CloudVpsProviderModel can be used to store data from the Terraform configuration.
type CloudVpsProviderModel struct {
	OsAuthUrl                     types.String `tfsdk:"os_auth_url"`
	OsApplicationCredentialId     types.String `tfsdk:"os_application_credential_id"`
	OsApplicationCredentialSecret types.String `tfsdk:"os_application_credential_secret"`
	OsProjectId                   types.String `tfsdk:"os_project_id"`
}

// CloudVpsProviderData is passed to the resources and data sources.
type CloudVpsProviderData struct {
	OsClient    *gophercloud.ProviderClient
	OsProjectId string
}

func (p *CloudVpsProvider) Metadata(ctx context.Context, req provider.MetadataRequest, resp *provider.MetadataResponse) {
	resp.TypeName = "cloudvps"
	resp.Version = p.version
}

func (p *CloudVpsProvider) Configure(ctx context.Context, req provider.ConfigureRequest, resp *provider.ConfigureResponse) {
	var data CloudVpsProviderModel
	diags := req.Config.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)

	if resp.Diagnostics.HasError() {
		return
	}

	authOpts := gophercloud.AuthOptions{
		IdentityEndpoint:            data.OsAuthUrl.ValueString(),
		ApplicationCredentialID:     data.OsApplicationCredentialId.ValueString(),
		ApplicationCredentialSecret: data.OsApplicationCredentialSecret.ValueString(),
	}

	osProvider, err := openstack.AuthenticatedClient(authOpts)
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to create client",
			fmt.Sprintf("Unable to create authenticated OpenStack client: %v", err),
		)
		return
	}

	d := &CloudVpsProviderData{
		OsClient:    osProvider,
		OsProjectId: data.OsProjectId.ValueString(),
	}
	resp.ResourceData = d
	resp.DataSourceData = d
}

func (p *CloudVpsProvider) Resources(ctx context.Context) []func() resource.Resource {
	return []func() resource.Resource{
		NewPuppetPrefixResource,
		NewWebProxyResource,
	}
}

func (p *CloudVpsProvider) DataSources(ctx context.Context) []func() datasource.DataSource {
	return []func() datasource.DataSource{}
}

func (p *CloudVpsProvider) Schema(ctx context.Context, req provider.SchemaRequest, resp *provider.SchemaResponse) {
	resp.Schema = schema.Schema{
		MarkdownDescription: "Provider to manage [Wikimedia Cloud VPS](https://wikitech.wikimedia.org/wiki/Portal:Cloud_VPS) features.",

		Attributes: map[string]schema.Attribute{
			"os_auth_url": schema.StringAttribute{
				MarkdownDescription: "OpenStack authentication URL",
				Required:            true,
			},
			"os_application_credential_id": schema.StringAttribute{
				MarkdownDescription: "OpenStack application credential ID",
				Required:            true,
			},
			"os_application_credential_secret": schema.StringAttribute{
				MarkdownDescription: "OpenStack application credential secret",
				Required:            true,
				Sensitive:           true,
			},
			"os_project_id": schema.StringAttribute{
				MarkdownDescription: "OpenStack project ID",
				Required:            true,
			},
		},
	}
}

func New(version string) func() provider.Provider {
	return func() provider.Provider {
		return &CloudVpsProvider{
			version: version,
		}
	}
}
