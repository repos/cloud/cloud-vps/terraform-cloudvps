package provider

import (
	"context"
	"fmt"
	"reflect"
	"sort"
	"strconv"

	"github.com/gophercloud/gophercloud"
	"github.com/hashicorp/terraform-plugin-framework-validators/listvalidator"
	"github.com/hashicorp/terraform-plugin-framework/attr"
	"github.com/hashicorp/terraform-plugin-framework/diag"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/int64planmodifier"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/planmodifier"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema/stringplanmodifier"
	"github.com/hashicorp/terraform-plugin-framework/schema/validator"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-framework/types/basetypes"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	"gopkg.in/yaml.v3"

	cloudvps "gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/clients"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/puppet"
)

// Ensure provider defined types fully satisfy framework interfaces
var _ resource.Resource = &PuppetPrefixResource{}
var _ resource.ResourceWithImportState = &PuppetPrefixResource{}

func NewPuppetPrefixResource() resource.Resource {
	return &PuppetPrefixResource{}
}

// PuppetPrefixResource defines the resource implementation.
type PuppetPrefixResource struct {
	client *puppet.Client
}

// PuppetPrefixResourceModel describes the resource data model.
type PuppetPrefixResourceModel struct {
	Id    types.Int64  `tfsdk:"id"`
	Name  types.String `tfsdk:"name"`
	Roles types.List   `tfsdk:"roles"`
	Hiera types.String `tfsdk:"hiera"`
}

func (m *PuppetPrefixResourceModel) createApiPrefix(diagnostics diag.Diagnostics) *puppet.Prefix {
	var roles []string
	if !m.Roles.IsNull() && !m.Roles.IsUnknown() {
		for _, elem := range m.Roles.Elements() {
			roles = append(roles, elem.(types.String).ValueString())
		}
	}

	hiera := map[string]any{}
	if !m.Hiera.IsNull() && !m.Hiera.IsUnknown() {
		err := yaml.Unmarshal([]byte(m.Hiera.ValueString()), &hiera)
		if err != nil {
			diagnostics.AddError(
				"Unable to parse hieradata",
				fmt.Sprintf("Unable to parse YAML: %v", err),
			)

			return nil
		}
	}

	return &puppet.Prefix{
		Id:    int(m.Id.ValueInt64()),
		Name:  m.Name.ValueString(),
		Roles: roles,
		Hiera: hiera,
	}
}

func (m *PuppetPrefixResourceModel) hasEqualHiera(prefix *puppet.Prefix, diagnostics diag.Diagnostics) bool {
	if m.Hiera.IsUnknown() {
		return false
	}

	data := make(map[string]any)
	err := yaml.Unmarshal([]byte(m.Hiera.ValueString()), &data)
	if err != nil {
		diagnostics.AddWarning(
			"Unable to parse configure Hiera data",
			fmt.Sprintf("This might result in unnecessary changes displayed. %v", err),
		)

		return false
	}

	return reflect.DeepEqual(prefix.Hiera, data)
}

func (m *PuppetPrefixResourceModel) readFrom(prefix *puppet.Prefix, diagnostics diag.Diagnostics) bool {
	if prefix == nil {
		diagnostics.AddError(
			"Unable to get prefix information",
			"Got a nil prefix",
		)
		return false
	}

	m.Id = types.Int64Value(int64(prefix.Id))
	m.Name = types.StringValue(prefix.Name)

	if prefix.Hiera != nil {
		data, err := yaml.Marshal(prefix.Hiera)
		if err != nil {
			diagnostics.AddError(
				"Unable to dump hieradata to string",
				fmt.Sprintf("Unable to dump YAML: %v", err),
			)
			return false
		}

		if !m.hasEqualHiera(prefix, diagnostics) {
			dataString := string(data)
			if dataString == "{}\n" {
				dataString = ""
			}

			m.Hiera = types.StringValue(dataString)
		}
	} else {
		m.Hiera = types.StringValue("")
	}

	var roles []attr.Value
	if prefix.Roles != nil {
		for _, role := range prefix.Roles {
			roles = append(roles, types.StringValue(role))
		}
	}
	var d diag.Diagnostics
	m.Roles, d = types.ListValue(types.StringType, roles)
	if d.HasError() {
		diagnostics.Append(d...)
		return false
	}

	return true
}

func (r *PuppetPrefixResource) Metadata(ctx context.Context, req resource.MetadataRequest, resp *resource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_puppet_prefix"
}

func (r *PuppetPrefixResource) Schema(ctx context.Context, req resource.SchemaRequest, resp *resource.SchemaResponse) {
	resp.Schema = schema.Schema{
		MarkdownDescription: "Manages a Cloud VPS [Puppet prefix](https://wikitech.wikimedia.org/wiki/Help:Puppet).",

		Attributes: map[string]schema.Attribute{
			"id": schema.Int64Attribute{
				Computed:            true,
				MarkdownDescription: "Internal prefix identifier",
				PlanModifiers: []planmodifier.Int64{
					int64planmodifier.UseStateForUnknown(),
				},
			},
			"name": schema.StringAttribute{
				MarkdownDescription: "Prefix",
				Required:            true,
				PlanModifiers: []planmodifier.String{
					EmptyUnderscore{},
					stringplanmodifier.RequiresReplace(),
				},
			},
			"roles": schema.ListAttribute{
				MarkdownDescription: "Assigned roles",
				ElementType:         types.StringType,
				Optional:            true,
				Validators: []validator.List{
					listvalidator.UniqueValues(),
					MustBeSorted{},
				},
			},
			"hiera": schema.StringAttribute{
				MarkdownDescription: "Assigned hieradata in YAML format",
				Optional:            true,
			},
		},
	}
}

// EmptyUnderscore is a plan modifier to format project-wide prefixes as required.
type EmptyUnderscore struct{}

func (e EmptyUnderscore) Description(ctx context.Context) string {
	return "Formats project-wide prefixes for the API"
}

func (e EmptyUnderscore) MarkdownDescription(ctx context.Context) string {
	return "Formats project-wide prefixes for the API"
}

func (e EmptyUnderscore) PlanModifyString(ctx context.Context, req planmodifier.StringRequest, resp *planmodifier.StringResponse) {
	if req.ConfigValue.ValueString() == "" {
		resp.PlanValue = types.StringValue("_")
	}
}

// MustBeSorted is a validator to ensure a list is sorted.
type MustBeSorted struct{}

func (m MustBeSorted) Description(ctx context.Context) string {
	return "Ensures lists are sorted"
}

func (m MustBeSorted) MarkdownDescription(ctx context.Context) string {
	return "Ensures lists are sorted"
}

func (m MustBeSorted) ValidateList(ctx context.Context, request validator.ListRequest, response *validator.ListResponse) {
	stringElements := []string{}
	for _, elem := range request.ConfigValue.Elements() {
		stringElements = append(stringElements, elem.(basetypes.StringValue).ValueString())
	}

	if !sort.StringsAreSorted(stringElements) {
		// TODO: can we automatically sort instead of erroring out?
		response.Diagnostics.AddAttributeError(
			request.Path,
			"Roles are not sorted",
			"Roles must be configured in alphabetical order.",
		)
	}
}

func (r *PuppetPrefixResource) Configure(ctx context.Context, req resource.ConfigureRequest, resp *resource.ConfigureResponse) {
	// Prevent panic if the provider has not been configured.
	if req.ProviderData == nil {
		return
	}

	data, ok := req.ProviderData.(*CloudVpsProviderData)
	if !ok {
		resp.Diagnostics.AddError(
			"Unexpected Resource Configure Type",
			fmt.Sprintf("Expected *CloudVpsProviderData, got: %T. Please report this issue to the provider developers.", req.ProviderData),
		)

		return
	}

	client, err := cloudvps.NewPuppetClient(
		data.OsClient,
		gophercloud.EndpointOpts{})

	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to create client",
			fmt.Sprintf("Unable to create Puppet ENC client: %v", err),
		)

		return
	}

	r.client = client
}

func (r *PuppetPrefixResource) Create(ctx context.Context, req resource.CreateRequest, resp *resource.CreateResponse) {
	var data *PuppetPrefixResourceModel

	// Read Terraform plan data into the model
	resp.Diagnostics.Append(req.Plan.Get(ctx, &data)...)

	if resp.Diagnostics.HasError() {
		return
	}

	prefix := data.createApiPrefix(resp.Diagnostics)
	if prefix == nil {
		return
	}

	createResult := r.client.CreatePrefix(*prefix)
	if createResult.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to create prefix",
			fmt.Sprintf("Unable to create a new prefix: %v", createResult.Err),
		)

		return
	}

	createdPrefix, err := createResult.Extract()
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to extract prefix data",
			fmt.Sprintf("Unable to extract prefix data: %v", err),
		)

		return
	}

	// The create API does not support setting hiera or class data (at least at the moment),
	// so do it with an update request afterwards
	prefix.Id = createdPrefix.Id

	updateResult := r.client.UpdatePrefix(*prefix)
	if updateResult.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to update prefix",
			fmt.Sprintf("Unable to update prefix %v: %v", prefix.Id, updateResult.Err),
		)

		return
	}

	updatedPrefix, err := updateResult.Extract()
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to extract prefix data",
			fmt.Sprintf("Unable to extract prefix data: %v", err),
		)

		return
	}

	ok := data.readFrom(updatedPrefix, resp.Diagnostics)
	if !ok {
		return
	}

	tflog.Trace(ctx, "created a prefix")
	resp.Diagnostics.Append(resp.State.Set(ctx, &data)...)
}

func (r *PuppetPrefixResource) Read(ctx context.Context, req resource.ReadRequest, resp *resource.ReadResponse) {
	var data *PuppetPrefixResourceModel

	// Read Terraform prior state data into the model
	resp.Diagnostics.Append(req.State.Get(ctx, &data)...)

	if resp.Diagnostics.HasError() {
		return
	}

	id := data.Id.ValueInt64()

	result := r.client.GetPrefixById(int(id))

	if result.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to read prefix",
			fmt.Sprintf("Unable to read prefix %v: %v", id, result.Err),
		)

		return
	}

	prefix, err := result.Extract()
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to extract prefix data",
			fmt.Sprintf("Unable to extract prefix data: %v", err),
		)

		return
	}

	ok := data.readFrom(prefix, resp.Diagnostics)
	if !ok {
		return
	}

	tflog.Trace(ctx, "read a prefix")
	resp.Diagnostics.Append(resp.State.Set(ctx, &data)...)
}

func (r *PuppetPrefixResource) Update(ctx context.Context, req resource.UpdateRequest, resp *resource.UpdateResponse) {
	var data *PuppetPrefixResourceModel

	// Read Terraform plan data into the model
	resp.Diagnostics.Append(req.Plan.Get(ctx, &data)...)

	if resp.Diagnostics.HasError() {
		return
	}

	prefix := data.createApiPrefix(resp.Diagnostics)
	if prefix == nil {
		return
	}

	result := r.client.UpdatePrefix(*prefix)
	if result.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to update prefix",
			fmt.Sprintf("Unable to update prefix %v: %v", prefix.Id, result.Err),
		)

		return
	}

	updatedPrefix, err := result.Extract()
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to extract prefix data",
			fmt.Sprintf("Unable to extract prefix data: %v", err),
		)

		return
	}

	ok := data.readFrom(updatedPrefix, resp.Diagnostics)
	if !ok {
		return
	}

	tflog.Trace(ctx, "updated a prefix")
	resp.Diagnostics.Append(resp.State.Set(ctx, &data)...)
}

func (r *PuppetPrefixResource) Delete(ctx context.Context, req resource.DeleteRequest, resp *resource.DeleteResponse) {
	var data *PuppetPrefixResourceModel

	// Read Terraform prior state data into the model
	resp.Diagnostics.Append(req.State.Get(ctx, &data)...)

	if resp.Diagnostics.HasError() {
		return
	}

	prefix := data.createApiPrefix(resp.Diagnostics)
	if prefix == nil {
		return
	}

	result := r.client.DeletePrefix(*prefix)
	if result.Err != nil {
		resp.Diagnostics.AddError(
			"Unable to delete prefix",
			fmt.Sprintf("Unable to delete prefix %v: %v", prefix.Id, result.Err),
		)

		return
	}
}

func (r *PuppetPrefixResource) ImportState(ctx context.Context, req resource.ImportStateRequest, resp *resource.ImportStateResponse) {
	id, err := strconv.ParseInt(req.ID, 10, 64)
	if err != nil {
		resp.Diagnostics.AddError(
			"Unable to parse identifier",
			fmt.Sprintf("Unable to parse %v as a number", req.ID),
		)

		return
	}

	diagnostics := resp.State.SetAttribute(ctx, path.Root("id"), id)
	resp.Diagnostics.Append(diagnostics...)
}
