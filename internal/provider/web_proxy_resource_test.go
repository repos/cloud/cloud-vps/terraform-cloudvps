package provider

import (
	"testing"

	"github.com/hashicorp/terraform-plugin-framework/attr"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/stretchr/testify/assert"
	"gitlab.wikimedia.org/repos/cloud/cloud-vps/go-cloudvps/pkg/webproxy"
)

func TestGetFqdn(t *testing.T) {
	model := webProxyModel{
		Hostname: types.StringValue("test"),
		Domain:   types.StringValue("codfw1dev.wmcloud.org"),
		Backends: types.ListValueMust(types.StringType, []attr.Value{
			types.StringValue("http://1.2.3.4"),
		}),
	}

	assert.Equal(t, "test.codfw1dev.wmcloud.org", getFqdn(model, &webproxy.Client{Client: nil}))
}

func TestProxyToData(t *testing.T) {
	proxy := webproxy.Proxy{
		Domain: "test.codfw1dev.wmcloud.org",
		Backends: []string{
			"http://1.2.3.4",
		},
	}

	data, diagnostics := proxyToData(proxy)

	assert.Equal(t, webProxyModel{
		Hostname: types.StringValue("test"),
		Domain:   types.StringValue("codfw1dev.wmcloud.org"),
		Backends: types.ListValueMust(types.StringType, []attr.Value{
			types.StringValue("http://1.2.3.4"),
		}),
	}, *data)

	assert.False(t, diagnostics.HasError())
}
