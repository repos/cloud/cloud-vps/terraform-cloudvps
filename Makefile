RESULT = out
DOCS = docs
DOCS_SITE = docs-site

.PHONY: clean build
all: clean build docs-site

clean:
	rm -rfv $(RESULT) $(DOCS) $(docs-site)

build:
	mkdir -pv $(RESULT)
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o $(RESULT)/terraform-provider-cloudvps

docs:
	go run github.com/hashicorp/terraform-plugin-docs/cmd/tfplugindocs generate --provider-name cloudvps --rendered-website-dir $(DOCS)

docs-site: docs
	mkdocs build -d $(DOCS_SITE)
